const WebSocket = require('ws');
const WebSocketServer = new WebSocket.Server({port: 8080});

var DataProvider = require('./DataProvider');
var Events = require('./Events');


WebSocketServer.on('connection', function connection(server) {

	server.serverIp = server._socket.remoteAddress;
	server.serverPort = server._socket.remotePort;

	server.identifier = server.serverIp + ":" + server.serverPort;

	server.requestId = 0;

	// Cписок клиентов и их имён
	// UserId => {'name' => ник, ...}
	server.UserIds;
	// Загружаем
	DataProvider.loadUserIds(server);


	console.log('Сервер ' + server.identifier + ' подключился!');

	// TODO: заменить
	server.getRequestId = function() {
		// Первый запрос '00000000-0000-0000-0000000000000000'
		id = '00000000-0000-0000-' + new Array(16 - (server.requestId + '').length).join('0') + server.requestId++;
		console.log(id);
		return id;
	}

	server.on('message', function(event) {
		Events.callEvent(event, server);
	});

	server.on('close', function close() {
		console.log('Сервер ' + server.identifier + ' отключился!');
		DataProvider.saveUserIds(server);
	});

	server.broadcastMessage = function(message) {
		command = {};
		command['body'] = {};
		command['body']['input'] = {};
		command['body']['input']['message'] = message;
		command['body']['origin'] = {};
		command['body']['origin']['type'] = 'chat';
		command['body']['name'] = 'say';
		command['body']['version'] = 1;
		command['body']['overload'] = 'default';
		command['header'] = {};
		command['header']['requestId'] = server.getRequestId();
		command['header']['messagePurpose'] = 'commandRequest';
		command['header']['version'] = 1;
		command['header']['messageType'] = 'commandRequest';
		command = JSON.stringify(command);
		server.send(command);
	}

	// Регестрируем эвенты
	Events.registerEvents(server);
	server.broadcastMessage('Подключение к серверу прошло успешно');
});

console.log('Запущен сервер для MCPE Realms на порту 8080');