var fs = require('fs');

/**
 * Получаем данные о игроках из файла
 */
var loadUserIds = function(server) {
	fs.exists('./servers/' + server.identifier, function(exists) {
		if (exists) {
			fs.readFile('./servers/' + server.identifier, function(error, data) {
				if (error) throw error;
				server.UserIds = JSON.parse(data);
			});
		}else {
			server.UserIds = {};
		}
	});
}

/**
 * Сохраняем данные о игроках в файл
 */
var saveUserIds = function(server) {
	data = JSON.stringify(server.UserIds);
	fs.writeFile('./servers/' + server.identifier , data, function(error) {
		if(error) throw error;
	});
}

module.exports.loadUserIds = loadUserIds;
module.exports.saveUserIds = saveUserIds;