var DataProvider = require('./DataProvider');

// Все эвенты, которые следует зарегестрировать
var events = ['PlayerMessage', 'MobKilled', 'BlockBroken', 'BlockPlaced'];

/**
 * Регестрируем эвенты
 * Для этого для каждого эвента создаем свой json запрос;
 */
registerEvents = function(server) {
	baseEvent = {};
	baseEvent['body'] = {};
	baseEvent['header'] = {};
	baseEvent['header']['messagePurpose'] = 'subscribe';
	baseEvent['header']['version'] = 1;
	baseEvent['header']['messageType'] = 'commandRequest';
	events.forEach(function(eventName) {
		event = baseEvent;
		event['body']['eventName'] = eventName;
		event['header']['requestId'] = server.getRequestId();
		data = JSON.stringify(event);
		server.send(data);
	});
}

onCommand = function(ClientId, command, args) {
	if (command == '~quest') {
		switch (args[0]) {
			case 'take':
				console.log('Берем квест');
				break;
			case 'info':
				console.log('Инфо');
				break;
			default:
				console.log('default');
		}
	}
}

onPlayerMessage = function(event, server) {
	// Игнорируем, если это commandResponse на наш commandRequest
	if (event['header']['messagePurpose'] == 'commandResponse') return;
	ClientId = event['body']['properties']['ClientId'];
	// Проверяем есть ли ник игрока в массиве с UserIds, и это ник не
	if (server.UserIds[ClientId] == undefined) {
		server.UserIds[ClientId] = {};
		server.UserIds[ClientId]['name'] = event['body']['properties']['Sender'];
		// Сохраняем UserIds, чтобы перестраховаться от случайных вылетов
		DataProvider.saveUserIds(server);
	}
	// Если это не команда - игнорируем
	if (msg[0] != '~') return;
	msg = event['body']['properties']['Message'].split[' '];
	onCommand(ClientId, msg.shift(), msg);
}

onMobKill = function(event, server) {
	// 10 - курочка, 11 - коровка, 12 - свинка, 13 - барашек, 23 - лошадка, 24 - ослик
	mobType = event['body']['properties']['MobType'];
}

onBlockBroken = function(event, server) {

}

onBlockPlace = function(event, server) {

}

callEvent = function(event, server) {
	console.log(event);
	event = JSON.parse(event);
	switch (event['body']['eventName']) {
		case 'PlayerMessage':
			onPlayerMessage(event, server);
			break;
		case 'MobKilled':
			onMobKill(event, server);
			break;
		case 'BlockBroken':
			onBlockBroken(event, server);
			break;
		case 'BlockPlaced':
			onBlockPlace(event, server);
	}
}

module.exports.registerEvents = registerEvents;
module.exports.callEvent = callEvent;